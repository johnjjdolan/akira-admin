<?php

// Report all PHP errors
error_reporting(E_ALL);

try {
    require('/var/akira/autoload.php');
    $akira = new \Akira\App();
    $akira->run();
} catch (Exception $e) {
    echo '(!) Akira Error:' . $e->getMessage() . "<br><hr>" . $e->getTraceAsString();
}